var config = require( "./config.json" );
 
var aws = require( "aws-sdk" );
aws.config.loadFromPath('./config.json');
var Q = require( 'q' );
var chalk = require( "chalk" );
var fs = require('fs');
var gm = require('gm').subClass({imageMagick: true});

var sqs = new aws.SQS({
    region: config.region,
    accessKeyId: config.accessKeyId,
    secretAccessKey: config.secretAccessKey,
    params: {
        QueueUrl: config.queueUrl
    }
});

var s3 = new aws.S3();
var dynamodb = new aws.DynamoDB.DocumentClient();
 
var receiveMessage = Q.nbind( sqs.receiveMessage, sqs );
var deleteMessage = Q.nbind( sqs.deleteMessage, sqs );
 
(function pollQueueForMessages() { 
    console.log( chalk.yellow( "Starting long-poll operation." ) );
    receiveMessage({
        WaitTimeSeconds: 3, 
        VisibilityTimeout: 10
    })
    .then(
        function handleMessageResolve( data ) {
            if ( ! data.Messages ) {
                throw(
                    workflowError(
                        "EmptyQueue",
                        new Error( "There are no messages to process." )
                    )
                ); 
            }
 
            console.log( chalk.green( "Deleting:", data.Messages[ 0 ].MessageId ) );
			console.log (chalk.green(data.Messages[ 0 ].Body));
			var strParam = data.Messages[ 0 ].Body.split("/");
			var paramsToLoad = {
				Bucket: "adud-"+strParam[0],
				Key: strParam[1]
			};
			var file = require('fs').createWriteStream('tmp/'+strParam[1]);
			var request = s3.getObject(paramsToLoad).createReadStream().pipe(file);
			request.on('finish', function (){
				console.log(chalk.green("Plik został zapisany na dysku"));
				gm('tmp/'+strParam[1])
				.implode(-1.2)
				.contrast(-6)				
				.autoOrient()
				.write('tmp/changed_'+strParam[1], function (err) {
				if (err) {
					console.log(err);
				}else{
				    console.log(chalk.green('Plik zostal przetworzony'));					
					var fileStream = require('fs').createReadStream('tmp/changed_'+strParam[1]);
					fileStream.on('open', function () {
						var paramsu = {
							Bucket: paramsToLoad.Bucket,
							Key: 'changed_'+strParam[1],
							ACL: 'public-read',
							Body: fileStream,
						};
						s3.putObject(paramsu, function(err, datau) {
    						if (err) {
    							console.log(err, err.stack);
    						}
    						else {
    							console.log(chalk.green(datau));
    							console.log(chalk.green('Upload zakonczony'));
    							var paramsdb = {
									TableName:"eluknyk_table",
							    Item:{
									"eluknyk":"Lukasz",
							        "value": 'changed_'+strParam[1],
							        "Replace": false
                                                            }
    							};
    							dynamodb.put(paramsdb, function(err, datass) {
    							if (err) {
    								console.log('Blad zapisu do bazy'+err, err.stack);
    							}
    							else {
    								console.log(chalk.green("Zapisano do bazy"));
    							}
    							});
    						}
						});
					});
				}
				});
			});
           
            return(
                deleteMessage({
                    ReceiptHandle: data.Messages[ 0 ].ReceiptHandle
                }) 
            ); 
        }
    )
    .then(
        function handleDeleteResolve( data ) { 
            console.log( chalk.green( "Message Deleted!" ) ); 
        }
    )
 
    .catch(
        function handleError( error ) { 
            switch ( error.type ) {
                case "EmptyQueue":
                    console.log( chalk.cyan( "Expected Error:", error.message ) );
                break;
                default:
                    console.log( chalk.red( "Unexpected Error:", error.message ) );
                break;
            } 
        }
    )

    .finally( pollQueueForMessages );
 
})();

function workflowError( type, error ) { 
    error.type = type; 
    return( error ); 
}
